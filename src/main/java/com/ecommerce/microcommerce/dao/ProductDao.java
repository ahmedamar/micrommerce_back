package com.ecommerce.microcommerce.dao;
import com.ecommerce.microcommerce.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface ProductDao extends JpaRepository<Product, Integer> {
    public List<Product>findAll();
    public Product findById(int id);
    public List<Product> findByPrixGreaterThan(int prixLimit);
    public List<Product> findByNomLike(String recherche);
    public Product save(Product product);

    //@Query(value = "SELECT * FROM Product p WHERE p.prix > :prixLimit", nativeQuery = true)
    //@Query(value = "SELECT new Product(id, nom, prix, prixAchat) FROM Product p WHERE p.prix > :prixLimit")
    @Query(value = "SELECT p FROM Product p WHERE p.prix > :prixLimit")
    List<Product> chercherUnProduitCher(@Param("prixLimit") int prix);
}