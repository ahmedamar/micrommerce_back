package com.ecommerce.microcommerce.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
public class Stock implements Serializable {

    @Id
    private int idS;
    private int qte;

    /*@OneToOne
    Product product;*/
    /*@OneToMany(mappedBy="stock")
    private Collection<Product> products ;*/
    @ManyToMany(mappedBy="stocks")
    private Collection<Product> products ;

    public Stock() {
    }

    public Stock(int idStock, int qte) {
        this.idS = idStock;
        this.qte = qte;
    }

    public int getIdStock() {
        return idS;
    }

    public void setIdStock(int idStock) {
        this.idS = idStock;
    }

    public int getQte() {
        return qte;
    }

    public void setQte(int qte) {
        this.qte = qte;
    }

    @Override
    public String toString() {
        return "Stock{" +
                "idStock=" + idS +
                ", qte=" + qte +
                '}';
    }
}
